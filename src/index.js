import React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';

class Menu extends Component {
  render() {
    return (
      <div>
        <table border="1px">
          <tbody>
          <tr>
            <td><NavLink to="/">Sebastians CV </NavLink></td>
            <td><NavLink to="/utdanning">Utdanning </NavLink></td>
            <td><NavLink to="/erfaring">Arbeidserfaring </NavLink></td>
            <td><NavLink to="/interesser">Interesser </NavLink></td>
          </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

class Utdanning extends Component {
  render() {
    return (
      <div>
        13 års skolegang og 1 år på Datateknologi-studiet ved UiB
      </div>
    );
  }
}

class Erfaring extends Component {
  render() {
    return (
      <div>
        4 år ved Rema 1000 Mogata
      </div>
    );
  }
}

class Interesser extends Component {
  render() {
    return (
      <div>
        Fotball, snowboarding og data
      </div>
    )
  }
}

/*class Menu2 extends Component {
  render() {
    return (
      <div>
        Menu:
        <a href="#/">Menu </a>
        <a href="#/Page1">Page 1 </a>
        <a href="#/Page2">Page 2 </a>
      </div>
    );
  }
}*/

class Home extends Component {
  render() {
    return <div>Sebastians CV</div>;
  }
}

class Page1 extends Component {
  render() {
    return <div>Page 1</div>;
  }
}

class Page2 extends Component {
  render() {
    return <div>Page 2</div>;
  }
}

class Hello extends Component {
  render() {
    return (
      <div>
        <b>Hello {this.props.who}</b>
      </div>
    );
  }
}

/*ReactDOM.render(
  <HashRouter>
    <div>
      <Hello who="Oslo" />
      <Menu />
      <Route exact path="/" component={Home} />
      <Route path="/page1" component={Page1} />
      <Route path="/page2" component={Page2} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);*/

ReactDOM.render(
  <HashRouter>
    <div>
      <Menu />
      <Route exact path="/" component={Home} />
      <Route path="/utdanning" component={Utdanning} />
      <Route path="/erfaring" component={Erfaring} />
      <Route path="/interesser" component={Interesser} />
    </div>
  </HashRouter>,
  document.getElementById("root")
);
